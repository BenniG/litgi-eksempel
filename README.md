# LitGi Eksempel

**Skattejagten**

Bruger til login:<br/>
email: test@test.dk <br/>
password: test12<br/>

Dette er IOS udgaven af vores projekt på 4. semester, hvor vi har skulle opsætte en app, der kunne give <br/>
social interaktion hos medarbejderne på TV2. <br/>
Jeg har stået for, hele opsætningen af IOS delen af vores projekt. <br/>
Vi arbejdede i fælleskab på et wireframe og her derefter, hver især, arbejdet med at får hver vores app til at ligne det så meget <br/>
som muligt. Mit udgangspunkt for at lære Swift, har været den grundlæggende viden jeg har haft med fra c#. Alt andet har jeg selv måtte <br/> tilegne mig, igennem internet og apples egen dokumentation.
