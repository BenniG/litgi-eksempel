//
//  Mission.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 24/04/2021.
//

import SwiftUI

//Missions data og deres type, i overenstemmelse med den type data der kommer fra api'et
struct Mission: Decodable, Identifiable{
    let title: String
    let description: String
    let pointValue: Int
    let missionType: String
    let id: String
    
}

