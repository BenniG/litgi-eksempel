//
//  ContentView.swift
//  Shared
//
//  Created by Benjamin Gestelev on 23/04/2021.
//

import SwiftUI
import Firebase

struct ContentView: View {
    @EnvironmentObject var network: Network
    @StateObject var viewRouter: ViewRouter
    
    var body: some View {
//   Kontrol af login status
        if Auth.auth().currentUser != nil {
//   Opsætning af view, hvis brugeren er logget ind
            GeometryReader { geometry in
                VStack{
                    Spacer()
//   Navigation via viewrouter
                    switch viewRouter.currentPage{
                    case .profile:
                        ProfileView(viewRouter: viewRouter)
                    case .missions:
                        NavigationView {
                            MissionRowView()
                        }
                    case .leaderboard:
                        Text("Hvem fører?")
                    case .login:
                        LoginView(viewRouter: viewRouter)
                    }
                    Spacer()
                    HStack{
//   Opsætning af tabbar, forbundet til viewrouter
                        TabBarIcon(viewRouter: viewRouter, assignedPage: .profile, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "person", tabName: "Profil")
                        
                        TabBarIcon(viewRouter: viewRouter, assignedPage: .missions, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "rosette", tabName: "Opgaver")
                        
                        TabBarIcon(viewRouter: viewRouter, assignedPage: .leaderboard, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "text.badge.star", tabName: "Leaderboard")
                    }
                    .frame(width: geometry.size.width, height: geometry.size.height/8 )
                    .background(Color("TabBarGray").shadow(radius: 2))
                    
                }
                .edgesIgnoringSafeArea(.bottom)
            }
        }else{
            LoginView(viewRouter: viewRouter)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewRouter: ViewRouter())
    }
}

struct TabBarIcon: View {
//    Opsætning af hvert enkelt icon til tabbar som subView
    @StateObject var viewRouter: ViewRouter
    let assignedPage: Page
    
    let width, height: CGFloat
    let systemIconName, tabName: String
    
    var body: some View {
        VStack{
            Image(systemName: systemIconName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: width, height: height)
                .padding(.top, 15)
            Text(tabName)
                .font(.footnote)
            Spacer()
        }
        .padding(.horizontal)
        .foregroundColor(viewRouter.currentPage == assignedPage ? Color("tv2rød"): Color("TabIconGray"))
        .onTapGesture {
            viewRouter.currentPage = assignedPage
        }
    }
}
