//
//  SkattejagtenV2App.swift
//  Shared
//
//  Created by Benjamin Gestelev on 23/04/2021.
//

import SwiftUI
import Firebase

@main
struct SkattejagtenV2App: App {
    init() {
        FirebaseApp.configure()
    }
    @StateObject var viewRouter = ViewRouter()
    var network = Network()
    
    
    var body: some Scene {
        WindowGroup {
            ContentView(viewRouter: viewRouter)
                .environmentObject(network)
        }
    }
}
