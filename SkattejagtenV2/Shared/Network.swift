//
//  Network.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 06/05/2021.
//

import SwiftUI

class Network: ObservableObject{
    @Published var missions: [Mission] = []

//Funktion til indhentning af missionsdata fra api
func getMissions(){

    guard let url = URL(string: "https://team5api.azurewebsites.net/v1.0/missions") else { fatalError("Missing URL")}

    let urlRequest = URLRequest(url:url)
    
    let dataTask = URLSession.shared.dataTask(with: urlRequest){(data, response, error) in
        if let error = error{
            print("Request error:",error)
            return
        }
        
        guard let response = response as? HTTPURLResponse else {return}
        
        if response.statusCode == 200{
            guard let data = data else {
                return
            }
            DispatchQueue.main.async {
                do{
//             afkodning af missioner fra json objekt
                    let decodeMissions = try JSONDecoder().decode([Mission].self, from: data)
                    self.missions = decodeMissions
                } catch let error{
                    print("Error decoding:", error)
                }
            }
        }
    }
    dataTask.resume()
}
}

