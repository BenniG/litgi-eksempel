//
//  ProfileView.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 24/04/2021.
//

import SwiftUI
import Firebase

struct ProfileView: View {
    
    @StateObject var viewRouter: ViewRouter
    
    var body: some View {
        VStack{
            Text("Sarah Sørensen")
                .font(.largeTitle)
                .fontWeight(.bold)
                .padding(.top, 10)
            Image("ProfilMock")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 150, height: 150)
                .clipped()
                .cornerRadius(150)
                .padding()
           Spacer()
            Text("Tasteperson hos It-Afdeling")
            Button(action: {
                logout()
            }){
                Text("Logout")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 220, height: 60)
                    .background(Color("tv2rød"))
                    .cornerRadius(35.0)
            }
            
        }
    }
//    logout funktion, sætter nuværende bruger til nil
  func logout(){
    try! Auth.auth().signOut()
    if Auth.auth().currentUser == nil{
        viewRouter.currentPage = .login
    }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(viewRouter: ViewRouter())
    }
}
