//
//  MissionRowView.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 24/04/2021.
//

import SwiftUI

struct MissionRowView: View {
    
    @EnvironmentObject var network:Network
    
    var body: some View {
//        Liste opsætning, henter liste af missioner fra api
//        og opsætter hver mission efter anvisning fra MissionCardView
        List(network.missions){ mission in
            MissionCardView(mission: mission)
                .aspectRatio(contentMode: .fill)
                
        }
        .navigationTitle(Text("Missioner"))
//        Henter missioner når viewet vises
        .onAppear{
            network.getMissions()
        }
    }
}

struct MissionRowView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MissionRowView()
                .environmentObject(Network())
        }
    }
}
