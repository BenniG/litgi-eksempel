//
//  LoginView.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 26/04/2021.
//

import SwiftUI
import Firebase

struct LoginView: View {
//   @State variable til opdatering af view
    @State var email = ""
    @State var password = ""

  @StateObject var viewRouter : ViewRouter
 
    var body: some View {
        VStack {
//     Opsætning af login via subViews
            WelcomeText()
            Tv2Logo()
            UsernameTextField(email: $email)
            PasswordSecureField(password: $password)
            Button(action:{ login()
        
                }) {
                    LoginButtonContent()
                }
        }
        .padding()
        
    }
//    Login funktion til firebase med den indtastede email
    func login(){
        Auth.auth().signIn(withEmail: email, password: password){
            (result, error) in
            if error != nil{
                print(error?.localizedDescription ?? "")
               
            }else{
                viewRouter.currentPage = .profile
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(viewRouter: ViewRouter())
    }
}
struct WelcomeText: View {
    var body: some View {
        Text("Skattejagten")
            .font(.largeTitle)
            .fontWeight(.semibold)
            .padding(.bottom, 20)
    }
}
struct Tv2Logo: View {
    var body: some View {
        Image("TV_2_RGB")
            .resizable()
            .frame(width: 150, height: 100)
            .padding(.bottom, 75)
    }
}

struct LoginButtonContent: View {
    var body: some View {
        Text("LOGIN")
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .frame(width: 220, height: 60)
            .background(Color("tv2rød"))
            .cornerRadius(35.0)
    }
}

struct UsernameTextField: View {
//    SubView med @binding til @state wrapper af samme navn
    @Binding var email: String
    var body: some View {
        TextField("Email", text: $email)
            .padding()
            .background(Color("TabBarGray"))
            .cornerRadius(20.0)
            .padding(.bottom, 20)
            .multilineTextAlignment(.center)
    }
}

struct PasswordSecureField: View {
    @Binding var password: String
    var body: some View {
        SecureField("Kodeord", text: $password)
            .padding()
            .background(Color("TabBarGray"))
            .cornerRadius(20.0)
            .padding(.bottom, 20)
            .multilineTextAlignment(.center)
    }
}
