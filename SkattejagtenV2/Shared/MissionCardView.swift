//
//  MissionCardView.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 24/04/2021.
//

import SwiftUI

struct MissionCardView: View {
    
    let mission: Mission

    var body: some View {
// Opsætning af view for et enkelt MissionCard
        HStack{
            Image(systemName: missionIcon(icon: mission.missionType))
                .resizable()
                .foregroundColor(Color("TabIconGray"))
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 50)
                .clipped()
                .cornerRadius(5)
            Spacer()
            Text(mission.title)
                .font(.title)
                .foregroundColor(Color("TabIconGray"))
                .fontWeight(.semibold)
                .padding(.horizontal)
                .aspectRatio(contentMode: .fill)
            Spacer()
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .padding()
        .overlay(
        RoundedRectangle(cornerRadius: 20)
            .stroke(Color("TabIconGray"), lineWidth: 3)
        )
        .aspectRatio(contentMode: .fill)
        
    }
// Funktion til bestemmelse af missions ikon alt efter missionType
    func missionIcon(icon: String)->String{
        var result: String
        switch icon {
        case "Quiz":
           result = "lightbulb"
        default:
            result = "person.2"
        }
        return result
    }
}

struct MissionCardView_Previews: PreviewProvider {
    static var previews: some View {
        MissionCardView(mission: missionData[0])
            .previewLayout(.sizeThatFits)
            
    }
}


