//
//  AuthorizationHelper.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 26/04/2021.
//

import SwiftUI
import Firebase

class CurrentUser: ObservableObject {
    @Published var user = Auth.auth().currentUser
}
