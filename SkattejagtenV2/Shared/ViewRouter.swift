//
//  ViewRouter.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 24/04/2021.
//

import SwiftUI
// Viewrouter med published variabel, der gør det muligt for alle views at observere Page
class ViewRouter: ObservableObject {
    @Published var currentPage: Page = .profile
}


enum Page {
    case profile
    case missions
    case leaderboard
    case login
}
