//
//  MissionData.swift
//  SkattejagtenV2
//
//  Created by Benjamin Gestelev on 24/04/2021.
//

import Foundation
//Eksempel data til MissionCardView og MissionRowView

let missionData: [Mission] = [
    Mission(title: "Hvem er hun?", description: "Du skal møde en fra regnskab!", pointValue: 5, missionType: "Quiz", id: ""),
    Mission(title: "Den ultimative kamp!", description: "Du skal spille en gang ping-pong med IT", pointValue: 5, missionType: "", id: ""),
    Mission(title: "Snapshot i køkkenet", description: "Tag et billede af en kaffekande", pointValue: 5, missionType: "", id: ""),
    Mission(title: "Hvem ved hvad?", description: "Kender du regnskab bedre end dine kollegaer? Lorem ipsum dolor sit amet, consectetur adipiscing elit", pointValue: 5, missionType: "", id: ""),
    Mission(title: "Find skatten", description: "Der er noget gemt i kopirummet", pointValue: 5, missionType: "", id: ""),
    Mission(title: "Hvem er han?", description: "Du skal møde en fra rengøring",  pointValue: 5, missionType: "", id: "")
]
